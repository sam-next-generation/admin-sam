<?php
/**
 * Created by PhpStorm.
 * User: rachmanareef
 * Date: 1/26/18
 * Time: 1:46 PM
 */

namespace common\models;


use yii\base\Model;

class Register extends Model
{
    public $username;
    public $name;
    public $password;
    public $email;
    public $phone;
    public $address;
    public $role;
    public $branch;
    public $salesType;

    public function rules()
    {
        return [
            [['username', 'name', 'password', 'email', 'phone', 'address', 'role', 'branch'],'required'],
            ['salesType','required', 'when' => function($model){
                return $this->role == 1;
            },
                'whenClient' => "function (attribute, value) { return $('#role_id').val() == 1; }"
            ],
            [['email'],'email']
        ];
    }

    public function attributeLabels()
    {
        return [
            ['username' => 'Username'],
            ['name' => 'Name'],
            ['password' => 'Password'],
            ['email' => 'Email'],
            ['phone' => 'Phone'],
            ['address' => 'Address'],
            ['role' => 'Role'],
            ['branch' => 'Branch'],
            ['salesType' => 'Type Sales']
        ];
    }


}