<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "prospect".
 *
 * @property string $prospect_id
 * @property string $branch_code
 * @property string $user_id
 * @property int $product_type
 * @property string $product__id
 * @property int $result_code
 * @property int $status_code
 * @property string $cust_id
 * @property string $prospect_crtdt
 * @property int $current_seq
 * @property string $target_nominal
 * @property int $prospect_type
 * @property bool $has_due_date
 * @property string $due_date
 * @property string $comp_id
 * @property int $bi_check
 * @property string $bi_note
 * @property int $activity_code
 * @property string $alifast_no
 * @property string $image_url
 * @property double $longitude
 * @property double $latitude
 *
 * @property Activity[] $activities
 * @property Customer $cust
 * @property RefProspectStatus $statusCode
 * @property ReffBranch $branchCode
 * @property ReffProspectType $prospectType
 * @property ReffResult $resultCode
 * @property SamUsers $user
 * @property ProspectApproval[] $prospectApprovals
 * @property SamUsers[] $users
 */
class CheckBooking extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'prospect';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['prospect_id', 'branch_code', 'user_id', 'result_code', 'status_code', 'cust_id', 'current_seq', 'prospect_type', 'comp_id'], 'required'],
            [['product_type', 'result_code', 'status_code', 'current_seq', 'target_nominal', 'prospect_type', 'bi_check', 'activity_code'], 'default', 'value' => null],
            [['product_type', 'result_code', 'status_code', 'current_seq', 'target_nominal', 'prospect_type', 'bi_check', 'activity_code'], 'integer'],
            [['prospect_crtdt', 'due_date'], 'safe'],
            [['has_due_date'], 'boolean'],
            [['longitude', 'latitude'], 'number'],
            [['prospect_id', 'user_id', 'cust_id'], 'string', 'max' => 40],
            [['branch_code'], 'string', 'max' => 7],
            [['product__id', 'comp_id'], 'string', 'max' => 4],
            [['bi_note', 'alifast_no'], 'string', 'max' => 100],
            [['image_url'], 'string', 'max' => 200],
            [['prospect_id'], 'unique'],
            [['cust_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customer::className(), 'targetAttribute' => ['cust_id' => 'cust_id']],
            [['status_code'], 'exist', 'skipOnError' => true, 'targetClass' => RefProspectStatus::className(), 'targetAttribute' => ['status_code' => 'status_code']],
            [['branch_code', 'comp_id'], 'exist', 'skipOnError' => true, 'targetClass' => ReffBranch::className(), 'targetAttribute' => ['branch_code' => 'branch_code', 'comp_id' => 'comp_id']],
            [['prospect_type'], 'exist', 'skipOnError' => true, 'targetClass' => ReffProspectType::className(), 'targetAttribute' => ['prospect_type' => 'prospect_type']],
            [['result_code'], 'exist', 'skipOnError' => true, 'targetClass' => ReffResult::className(), 'targetAttribute' => ['result_code' => 'result_code']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => SamUsers::className(), 'targetAttribute' => ['user_id' => 'user_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'prospect_id' => 'Prospect ID',
            'branch_code' => 'Branch Code',
            'user_id' => 'User ID',
            'product_type' => 'Product Type',
            'product__id' => 'Product  ID',
            'result_code' => 'Result Code',
            'status_code' => 'Status Code',
            'cust_id' => 'Cust ID',
            'prospect_crtdt' => 'Tgl Prospek',
            'current_seq' => 'Current Seq',
            'target_nominal' => 'Target Nominal',
            'prospect_type' => 'Prospect Type',
            'has_due_date' => 'Has Due Date',
            'due_date' => 'Due Date',
            'comp_id' => 'Comp ID',
            'bi_check' => 'Bi Check',
            'bi_note' => 'Bi Note',
            'activity_code' => 'Activity Code',
            'alifast_no' => 'Alifast No',
            'image_url' => 'Image Url',
            'longitude' => 'Longitude',
            'latitude' => 'Latitude',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivities()
    {
        return $this->hasMany(Activity::className(), ['prospect_id' => 'prospect_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCust()
    {
        return $this->hasOne(Customer::className(), ['cust_id' => 'cust_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatusCode()
    {
        return $this->hasOne(RefProspectStatus::className(), ['status_code' => 'status_code']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBranchCode()
    {
        return $this->hasOne(ReffBranch::className(), ['branch_code' => 'branch_code', 'comp_id' => 'comp_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProspectType()
    {
        return $this->hasOne(ReffProspectType::className(), ['prospect_type' => 'prospect_type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResultCode()
    {
        return $this->hasOne(ReffResult::className(), ['result_code' => 'result_code']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(SamUsers::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProspectApprovals()
    {
        return $this->hasMany(ProspectApproval::className(), ['prospect_id' => 'prospect_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(SamUsers::className(), ['user_id' => 'user_id'])->viaTable('prospect_approval', ['prospect_id' => 'prospect_id']);
    }
}
