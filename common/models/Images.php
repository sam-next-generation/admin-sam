<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "images".
 *
 * @property string $image_id
 * @property string $image_ext
 * @property resource $image_data
 * @property string $crtdt
 */
class Images extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'images';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['image_id'], 'required'],
            [['image_data'], 'string'],
            [['crtdt'], 'safe'],
            [['image_id'], 'string', 'max' => 50],
            [['image_ext'], 'string', 'max' => 10],
            [['image_id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'image_id' => 'Image ID',
            'image_ext' => 'Image Ext',
            'image_data' => 'Image Data',
            'crtdt' => 'Crtdt',
        ];
    }
}
