<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\RefActivity;

/**
 * RefActivitySearch represents the model behind the search form of `common\models\RefActivity`.
 */
class RefActivitySearch extends RefActivity
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['activity_code'], 'integer'],
            [['activity_name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RefActivity::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'activity_code' => $this->activity_code,
        ]);

        $query->andFilterWhere(['ilike', 'activity_name', $this->activity_name]);

        return $dataProvider;
    }
}
