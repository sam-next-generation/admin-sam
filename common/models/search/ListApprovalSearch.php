<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\CheckBooking;

/**
 * CheckBookingSearch represents the model behind the search form of `common\models\CheckBooking`.
 */
class ListApprovalSearch extends CheckBooking
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['prospect_id', 'branch_code', 'user_id', 'product__id', 'cust_id', 'prospect_crtdt', 'due_date', 'comp_id', 'bi_note', 'alifast_no', 'image_url'], 'safe'],
            [['product_type', 'result_code', 'status_code', 'current_seq', 'target_nominal', 'prospect_type', 'bi_check', 'activity_code'], 'integer'],
            [['has_due_date'], 'boolean'],
            [['longitude', 'latitude'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$i)
    {
        $query = CheckBooking::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'product_type' => $this->product_type,
            'result_code' => $this->result_code,
            'status_code' => $this->status_code,
            'prospect_crtdt' => $this->prospect_crtdt,
            'current_seq' => $this->current_seq,
            'target_nominal' => $this->target_nominal,
            'prospect_type' => $this->prospect_type,
            'has_due_date' => $this->has_due_date,
            'due_date' => $this->due_date,
            'bi_check' => $this->bi_check,
            'activity_code' => $this->activity_code,
            'longitude' => $this->longitude,
            'latitude' => $this->latitude,
        ]);

        $query->andFilterWhere(['ilike', 'prospect_id', $this->prospect_id])
            ->andFilterWhere(['ilike', 'branch_code', $this->branch_code])
            ->andFilterWhere(['ilike', 'user_id', $this->user_id])
            ->andFilterWhere(['ilike', 'product__id', $this->product__id])
            ->andFilterWhere(['ilike', 'cust_id', $this->cust_id])
            ->andFilterWhere(['ilike', 'comp_id', $this->comp_id])
            ->andFilterWhere(['ilike', 'bi_note', $this->bi_note])
            ->andFilterWhere(['ilike', 'alifast_no', $this->alifast_no])
            ->andFilterWhere(['ilike', 'image_url', $this->image_url]);

        if($i == 1){
            $query->andFilterWhere(['status_code' => 3]);
        }

        if($i==2){
            $query->andFilterWhere(['status_code' => 5]);
        }
        return $dataProvider;
    }
}
