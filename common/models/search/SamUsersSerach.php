<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\SamUsers;

/**
 * SamUsersSerach represents the model behind the search form about `common\models\SamUsers`.
 */
class SamUsersSerach extends SamUsers
{
    /**
     * @inheritdoc
     */
    public $company;
    public $branch;
    public $role;

    public function rules()
    {
        return [
            [['user_id', 'email', 'passwd', 'nama', 'address', 'phone_no', 'fbs_token', 'profile_pic', 'fcm_token', 'token', 'branch_code', 'comp_id','company','branch','role'], 'safe'],
            [['role_id', 'user_status', 'sales_type'], 'integer'],
            [['cur_lat', 'cur_lon'], 'number'],
            [['active'], 'boolean'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'user_id' => 'Username',
            'company' => 'Company',
            'branch'  => 'Branch',
            'role' => 'Role'
        ];
    }


    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SamUsers::find();
        $session = Yii::$app->session;
        $query->joinWith(['company','branch','role']);


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['company'] = [
            'asc' => ['company.company_name' => SORT_ASC],
            'desc' => ['company.company_name' => SORT_DESC]
        ];

        $dataProvider->sort->attributes['branch'] = [
            'asc' => ['reff_branch.branch_name' => SORT_ASC],
            'desc' => ['reff_branch.branch_name' => SORT_DESC]
        ];

        $dataProvider->sort->attributes['role'] = [
            'asc' => ['reff_user_role.role_description' => SORT_ASC],
            'desc' => ['reff_user_role.role_description' => SORT_DESC]
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(['sam_users.comp_id' => $session->get('comp_id')])
            ->andFilterWhere(['<>','user_id', Yii::$app->user->identity->username]);

        // grid filtering conditions
        $query->andFilterWhere([
            'role_id' => $this->role_id,
            'cur_lat' => $this->cur_lat,
            'cur_lon' => $this->cur_lon,
            'active' => $this->active,
            'user_status' => $this->user_status,
            'sales_type' => $this->sales_type,
        ]);

        $query->andFilterWhere(['sam_users.active' => true]);

        $query->andFilterWhere(['like', 'user_id', $this->user_id])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'passwd', $this->passwd])
            ->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'phone_no', $this->phone_no])
            ->andFilterWhere(['like', 'fbs_token', $this->fbs_token])
            ->andFilterWhere(['like', 'profile_pic', $this->profile_pic])
            ->andFilterWhere(['like', 'fcm_token', $this->fcm_token])
            ->andFilterWhere(['like', 'token', $this->token])
            ->andFilterWhere(['like', 'branch_code', $this->branch_code])
            ->andFilterWhere(['like', 'comp_id', $this->comp_id])
            ->andFilterWhere(['like', 'company.company_id', $this->company])
            ->andFilterWhere(['like', 'reff_branch.branch_code', $this->branch])
            ->andFilterWhere(['reff_user_role.role_id'=> $this->role]);

        return $dataProvider;
    }
}
