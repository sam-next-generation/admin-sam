<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "product".
 *
 * @property int $product_type
 * @property string $product__id
 * @property string $company_id
 * @property string $product_def
 * @property int $sales_type
 *
 * @property Company $company
 * @property ReffProductType $productType
 */
class Product extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_type', 'product__id', 'company_id', 'product_def'], 'required'],
            [['product_type', 'sales_type'], 'default', 'value' => null],
            [['product_type', 'sales_type'], 'integer'],
            [['product__id', 'company_id'], 'string', 'max' => 4],
            [['product_def'], 'string', 'max' => 40],
            [['product_type', 'product__id'], 'unique', 'targetAttribute' => ['product_type', 'product__id']],
            [['product_type', 'product__id', 'company_id'], 'unique', 'targetAttribute' => ['product_type', 'product__id', 'company_id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'company_id']],
            [['product_type'], 'exist', 'skipOnError' => true, 'targetClass' => ReffProductType::className(), 'targetAttribute' => ['product_type' => 'product_type']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_type' => 'Product Type',
            'product__id' => 'Product  ID',
            'company_id' => 'Company ID',
            'product_def' => 'Product Def',
            'sales_type' => 'Sales Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['company_id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductType()
    {
        return $this->hasOne(ReffProductType::className(), ['product_type' => 'product_type']);
    }
}
