<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "ref_bicheck".
 *
 * @property int $bi_check
 * @property string $bi_def
 */
class RefBicheck extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ref_bicheck';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bi_check'], 'required'],
            [['bi_check'], 'default', 'value' => null],
            [['bi_check'], 'integer'],
            [['bi_def'], 'string', 'max' => 15],
            [['bi_check'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'bi_check' => 'Bi Check',
            'bi_def' => 'Bi Def',
        ];
    }
}
