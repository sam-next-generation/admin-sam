<?php
/**
 * Created by PhpStorm.
 * User: rachmanareef
 * Date: 2/20/18
 * Time: 8:54 AM
 */

namespace common\models;


use yii\base\Model;

class Alifast extends Model
{
    public $alifast_no;

    public function rules()
    {
        return
            [
                [['alifast_no'], 'required']
            ];
    }

    public function attributeLabels()
    {
        return ['alifast_no' => 'Nomor Alifast'];
    }

    /**
     * @return mixed
     */
    public function getAlifastNo()
    {
        return $this->alifast_no;
    }

    /**
     * @param mixed $alifast_no
     */
    public function setAlifastNo($alifast_no)
    {
        $this->alifast_no = $alifast_no;
    }




}