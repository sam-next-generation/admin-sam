<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "ref_activity".
 *
 * @property int $activity_code
 * @property string $activity_name
 */
class RefActivity extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ref_activity';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['activity_code'], 'required'],
            [['activity_code'], 'default', 'value' => null],
            [['activity_code'], 'integer'],
            [['activity_name'], 'string', 'max' => 100],
            [['activity_code'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'activity_code' => 'Activity Code',
            'activity_name' => 'Activity Name',
        ];
    }
}
