<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ReffResult */

$this->title = 'Update Reff Result';
$this->params['breadcrumbs'][] = ['label' => 'Reff Results', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->result_code, 'url' => ['view', 'id' => $model->result_code]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="reff-result-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
