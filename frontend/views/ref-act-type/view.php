<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ReffActType */

$this->title = $model->act_type;
$this->params['breadcrumbs'][] = ['label' => 'Reff Act Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reff-act-type-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->act_type], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->act_type], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'act_type',
            'act_type_def',
        ],
    ]) ?>

</div>
