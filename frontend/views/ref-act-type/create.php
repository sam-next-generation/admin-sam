<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ReffActType */

$this->title = 'Create Reff Act Type';
$this->params['breadcrumbs'][] = ['label' => 'Reff Act Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reff-act-type-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
