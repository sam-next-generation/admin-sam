<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ReffSalesType */

$this->title = 'Create Reff Sales Type';
$this->params['breadcrumbs'][] = ['label' => 'Reff Sales Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reff-sales-type-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
