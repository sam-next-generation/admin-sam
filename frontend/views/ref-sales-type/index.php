<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\ReffSalesTypeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Reff Sales Type';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reff-sales-type-index">


    <p>
        <?= Html::a('Create', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'sales_type_def',
            'company.company_name',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
