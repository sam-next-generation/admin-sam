<?php
/**
 * Created by PhpStorm.
 * User: rachmanareef
 * Date: 1/26/18
 * Time: 2:00 PM
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use common\models\SamRole;
use common\models\Branch;
use common\models\SalesType;

$this->title = 'Create User';
$this->params['breadcrumbs'][] = ['label' => 'Sam Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$session = Yii::$app->session;
?>
<?php
$this->registerJs(<<< EOT_JS_CODE

$(document).ready(function(){

    $('#register-role').change(function(){
        if($(this).val() == 1){
            $('.field-register-salestype').show();
        }else{
            $('.field-register-salestype').hide();
        }
    });
});

EOT_JS_CODE
);
?>
<?php $form = ActiveForm::begin(); ?>
<?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
<?= $form->field($model, 'password')->passwordInput() ?>
<?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
<?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
<?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
<?= $form->field($model, 'role')->widget(Select2::className(),[
    'id' => 'role_id',
    'model' => $model,
    'attribute' => 'product_type',
    'data' => ArrayHelper::map(SamRole::find()->all(),'role_id','role_description'),
    'options' => ['placeholder' => 'Choose Role'],
    'pluginOptions' => [
        'allowClear' => true
    ]
]) ?>
<?= $form->field($model, 'branch')->widget(Select2::className(),[
    'model' => $model,
    'attribute' => 'branch',
    'data' => ArrayHelper::map(Branch::find()->where(['comp_id' => $session->get('comp_id')])->all(),'branch_code','branch_name'),
    'options' => ['placeholder' => 'Choose Branch'],
    'pluginOptions' => [
        'allowClear' => true
    ]
]) ?>
<?= $form->field($model, 'salesType')->widget(Select2::className(),[
    'id' => 'sales_type',
    'model' => $model,
    'attribute' => 'salesType',
    'data' => ArrayHelper::map(SalesType::find()->where(['comp_id' => $session->get('comp_id')])->all(),'sales_type','sales_type_def'),
    'options' => ['placeholder' => 'Choose Sales Type'],
    'pluginOptions' => [
        'allowClear' => true
    ]
]) ?>

<div class="form-group">
    <?= Html::submitButton('Create', ['class' => 'btn btn-primary']) ?>
</div>

<?php ActiveForm::end(); ?>
