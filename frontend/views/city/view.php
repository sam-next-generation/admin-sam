<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\City */

$this->title = $model->province_code;
$this->params['breadcrumbs'][] = ['label' => 'Cities', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="city-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'province_code' => $model->province_code, 'city_code' => $model->city_code], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'province_code' => $model->province_code, 'city_code' => $model->city_code], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'province_code',
            'city_code',
            'city_name',
            'rank',
        ],
    ]) ?>

</div>
