<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\BiCheck */

$this->title = 'Update Bi Check';
$this->params['breadcrumbs'][] = ['label' => 'Bi Checks', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->prospect_id, 'url' => ['view', 'id' => $model->prospect_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="bi-check-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
