<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use kartik\sidenav\SideNav;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use \common\models\SamUsers;
use \common\models\SamRole;
use common\models\Company;
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link rel="icon" href="<?=Yii::$app->request->getBaseUrl()?>/images/sam-icon.png" type="image/x-icon">
    <?php $this->head() ?>
</head>
<body id="minovate" class="appWrapper device-lg scheme-default default-scheme-color header-fixed aside-fixed rightbar-hidden">
<?php $this->beginBody() ?>
<div id="wrap">

    <section id="header">
        <header class="clearfix">
            <div class="branding">
                <!-- <a class="brand" href="/rbb-bjbs/">
                </a> -->
                <a  href="home">
                    <center></center>
                    <?php $base = Yii::$app->basePath?>
                    <!--<img src="<?/*=$base*/?>/web/images/sam.png" style="    width: 70px;margin-left: 25%;margin-top: -12px;" align="center">-->
                    <?=Html::img("@web/images/sam.png",["style" => "width: 70px;margin-left: 25%;margin-top: -12px;","align" => "center"])?>
                </a>
                <a role="button" tabindex="0" class="offcanvas-toggle visible-xs-inline"><i class="fa fa-bars"></i></a>
            </div>
            <?php

                $session = Yii::$app->session;
                $user = SamUsers::find()->where(['user_id' => Yii::$app->user->identity->username])->one();
                $role = SamRole::find()->where(['role_id' => $user->role_id])->one();
                $company = Company::find()->where(['company_id' => $user->comp_id])->one();
            ?>

            <ul class="nav-right pull-right list-inline">
                <li class="dropdown nav-profile">
                    <a href class="dropdown-toggle" data-toggle="dropdown">
                        <span> <?=Yii::$app->user->identity->username?> <i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul class="dropdown-menu pull-right panel panel-default animated littleFadeInUp" role="menu">
                        <li>
                            <a role="button" href="" tabindex="0">
                                <b>Name </b>: <?=$user->nama?>
                            </a>
                        </li>
                        <li>
                            <a role="button" href="" tabindex="0">
                                <b>Role </b>: <?=$role->role_description?>
                            </a>
                        </li>
                        <li>
                            <a role="button" href="" tabindex="0">
                                <b>Company </b>: <?=$company->company_name?>
                            </a>
                        </li>
                        <hr>
                        <li>
                            <?=Html::beginForm(['/site/logout'], 'post')?>
                                <?=Html::submitButton(
                                    '<i class="fa fa-sign-out"></i>Logout'
                                )?>
                            <?=Html::endForm()?>

                        </li>
                    </ul>
                </li>
            </ul>

        </header>
    </section>

    <!-- <?php
    $menuItems = [
        ['label' => 'Home', 'url' => ['/site/index']],
        ['label' => 'Branch', 'url' => ['/branch']],
        ['label' => 'Company', 'url' => ['/company']],
        ['label' => 'User', 'url' => ['/sam-user']],

        ['label' => 'Ref Activity', 'url' => ['/ref-activity']],
        ['label' => 'Ref Action Type', 'url' => ['/ref-act-type']],
        // ['label' => 'Ref Result Type', 'url' => ['/ref-result']],
        ['label' => 'Ref Sales Type', 'url' => ['/ref-sales-type']],
        ['label' => 'Ref Product Type', 'url' => ['/ref-product-type']],
        ['label' => 'Ref Product', 'url' => ['/product']],
        ['label' => 'Ref City', 'url' => ['/city']],
        ['label' => 'Ref Company Type', 'url' => ['/company-type']],

        ['label' => 'BI Check', 'url' => ['/bi-check']],
        ['label' => 'Check Booking', 'url' => ['/check-booking']],
        ['label' => 'Report Bi Check', 'url' => ['/report-bi-check']],
        ['label' => 'Report Approval', 'url' => ['/check-booking/list-approval']],
        ['label' => 'Report Rejected', 'url' => ['/check-booking/list-rejected']],

    ];
    $branch = $session->get('branch_code');
    $role = $session->get('role_id');
    ?>
 -->

    <div id="controls">
        <aside id="sidebar">
            <div id="sidebar-wrap">
                <div class="panel-group slim-scroll" role="tablist">
                    <div class="panel panel-default">
                        <div id="sidebarNav" class="panel-collapse collapse in" role="tabpanel">
                            <div class="panel-body">

                                <!-- NAVIGATION MENU -->
                                <ul id="navigation">
                                    <li class="">
                                        <?=Html::a('<i class="fa fa-dashboard"></i> <span>Dashboard</span>',['site/'])?>
                                    </li>
                                    <?php
                                    if($branch == '000' && $role == '0'){
                                    ?>
                                    <li class="">
                                        <?=Html::a('<i class=" fa fa-sitemap"></i> <span>Branch</span>',['branch/'])?>
                                    </li>


                                    <li class="">
                                        <?=Html::a('<i class="fa fa-user"></i> <span>Users</span>',['sam-user/'])?>
                                    </li>
                                        <?php
                                    }
                                    ?>
                                    <li class="">
                                        <?=Html::a('<i class="fa fa-btc"></i> <span>BI Check</span>',['bi-check/'])?>
                                    </li>
                                    <li class="">
                                        <?=Html::a('<i class="fa fa-leanpub"></i> <span>Check Booking</span>',['check-booking/'])?>
                                    </li>

                                    <li class="">
                                        <a href="#"><i class="icon-folder"></i> <span>Laporan</span></a>
                                        <ul>
                                            <li class="">
                                                <?=Html::a(' <i class="icon-pencil"></i> <span>Laporan Approval</span>',['check-booking/list-approval'])?>
                                            </li>
                                            <li class="">
                                                <?=Html::a(' <i class="icon-pencil"></i> <span>Laporan Rejection</span>',['check-booking/list-rejected'])?>
                                            </li>
                                        </ul>
                                    </li>

                                </ul>
                                <!-- END MENU -->
                               

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </aside>
    </div>

    <section id="content">
        <div class="page ">
            <div class="pageheader">
                <h1 class="custom-font"><?=$this->title?></h1>
                 <div class="page-bar">
                    <?= Breadcrumbs::widget([
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ]) ?>
                </div>

            </div>
            <?= $content ?>
        </div>
    </section>
</div>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
