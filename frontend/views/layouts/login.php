<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use kartik\sidenav\SideNav;
use frontend\assets\AppAsset;
use common\widgets\Alert;
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link rel="icon" href="<?=Yii::$app->request->getBaseUrl()?>/images/sam-icon.png" type="image/x-icon">
    <?php $this->head() ?>
</head>
<body id="minovate" class="appWrapper device-lg scheme-default default-scheme-color header-fixed aside-fixed rightbar-hidden">
<?php $this->beginBody() ?>

<div id="wrap">
    <div class="page page-core page-login">
        <div class="text-center">
            <?=Html::img("@web/images/login_sam.png",["style" => "width: 13%"])?>
        </div>
        <div class="container w-420 p-20 bg-white mt-40 text-center">
            <div id="cssload-overlay" class="hide">
                <div class="cssload-loading"></div>
            </div>
            <?= $content ?>
        </div>
    </div>


</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
