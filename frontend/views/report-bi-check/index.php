<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\grid\GridView;
/* @var $this yii\web\View */
/* @var $searchModel common\models\ReportBiCheckSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Report Bi Check';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="report-bi-check-index">

    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>
    <?= GridView::widget([
        'id' => 'kv-grid-demo',
        'dataProvider' => $dataProvider,
        'responsive' => true,
        'pjax' => true,
        'pjaxSettings' => [
            'neverTimeout' => true
        ],
        'resizableColumns' => true,
        'floatHeader' => true,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'header' => 'No'
            ],
            'branchCode.branch_name',
            'cust.identification_no',
            'cust.cust_name',
            'cust.cust_address',
            'target_nominal',
            'prospect_crtdt',
            /*[
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Action'
            ],*/
        ],
    ]); ?>
</div>
