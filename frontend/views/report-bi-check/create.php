<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ReportBiCheck */

$this->title = 'Create Report Bi Check';
$this->params['breadcrumbs'][] = ['label' => 'Report Bi Checks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="report-bi-check-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
