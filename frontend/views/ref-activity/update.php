<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\RefActivity */

$this->title = 'Update Ref Activity';
$this->params['breadcrumbs'][] = ['label' => 'Ref Activities', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->activity_code, 'url' => ['view', 'id' => $model->activity_code]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ref-activity-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
