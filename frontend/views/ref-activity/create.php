<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\RefActivity */

$this->title = 'Create Ref Activity';
$this->params['breadcrumbs'][] = ['label' => 'Ref Activities', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-activity-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
