<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\RefActivity */

$this->title = $model->activity_code;
$this->params['breadcrumbs'][] = ['label' => 'Ref Activities', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-activity-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->activity_code], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->activity_code], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'activity_code',
            'activity_name',
        ],
    ]) ?>

</div>
