<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\ReffProductTypeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Reff Product Type';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reff-product-type-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'product_type_def',
            'company.company_name',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
