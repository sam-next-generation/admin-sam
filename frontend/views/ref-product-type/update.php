<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ReffProductType */

$this->title = 'Update Reff Product Type';
$this->params['breadcrumbs'][] = ['label' => 'Reff Product Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->product_type, 'url' => ['view', 'id' => $model->product_type]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="reff-product-type-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
