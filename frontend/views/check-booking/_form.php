<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\CheckBooking */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="check-booking-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'bi_check')->widget(Select2::className(),[
            'model' => $model,
            'attribute' => 'bi_check',
            'data' => ArrayHelper::map(\common\models\RefBicheck::find()->where(['<>','bi_check', 0])->all(),'bi_check','bi_def'),
            'options' => ['placeholder' => 'Choose Bi Check'],
            'pluginOptions' => [
                    'allowClear' => true
            ]
    ]) ?>

    <?= $form->field($model, 'bi_note')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
