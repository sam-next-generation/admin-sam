<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CheckBooking */

$this->title = 'Create Check Booking';
$this->params['breadcrumbs'][] = ['label' => 'Check Bookings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="check-booking-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
