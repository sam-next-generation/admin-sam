<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CheckBooking */

$this->title = 'Update Check Booking';
$this->params['breadcrumbs'][] = ['label' => 'Check Bookings', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->prospect_id, 'url' => ['view', 'id' => $model->prospect_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="check-booking-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
