<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
$this->title = 'Rejection Alifast';
$this->params['breadcrumbs'][] = ['label' => 'Check Booking', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $form = ActiveForm::begin(); ?>
<?= $form->field($model, 'alifast_no')->textInput(['maxlength' => true]) ?>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        <?= Html::a('Cancel',['check-booking/'],['class' => 'btn btn-danger'])?>
    </div>
<?php ActiveForm::end(); ?>