<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use kartik\file\FileInput;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\Company */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="company-form">

    <?php 
      $form = ActiveForm::begin([
          'options'=>['enctype'=>'multipart/form-data']]); // important         
           ?>

   <?= $form->field($model, 'company_id')->textInput(['maxlength' => true]) ?>

   <?= $form->field($model, 'company_name')->textInput(['maxlength' => true]) ?>

   <?= $form->field($model, 'comp_email')->textInput(['maxlength' => true]) ?>

   <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

   <?= $form->field($model, 'company_type')->widget(Select2::className(),[
            'model' => $model,
            'attribute' => 'company_type',
            'data' => ArrayHelper::map(\common\models\ReffCompanyType::find()->all(),'company_type','company_type_def'),
            'options' => ['placeholder' => 'Choose Sales Type'],
            'pluginOptions' => [
                    'allowClear' => true,
                    'format' => 'yyyy-mm-dd'
            ]
    ]) ?>

    <?=
        $form->field($model, 'contract_start')->widget(DatePicker::classname(), [
            'options' => ['placeholder' => 'Input Date ...'],
            'pluginOptions' => [
                'autoclose'=>true,
                'format' => 'yyyy-mm-dd'
            ]
        ]);
    ?>

    <?=
        $form->field($model, 'contract_end')->widget(DatePicker::classname(), [
            'options' => ['placeholder' => 'Input Date ...'],
            'pluginOptions' => [
                'autoclose'=>true,
                'format' => 'yyyy-mm-dd'
            ]
        ]);
    ?>

    <?= $form->field($model, 'bill_status')->widget(Select2::className(),[
            'model' => $model,
            'attribute' => 'bill_status',
            'data' => ArrayHelper::map(\common\models\ReffBillStatus::find()->all(),'bill_status','bill_status_def'),
            'options' => ['placeholder' => 'Choose Bil Status'],
            'pluginOptions' => [
                    'allowClear' => true
            ]
    ]) ?>

    <?= $form->field($model, 'billing_type')->widget(Select2::className(),[
            'model' => $model,
            'attribute' => 'billing_type',
            'data' => ArrayHelper::map(\common\models\ReffBill::find()->all(),'billing_type','price'),
            'options' => ['placeholder' => 'Choose Biling Type'],
            'pluginOptions' => [
                    'allowClear' => true
            ]
    ]) ?>

    <?=
        $form->field($model, 'billing_date')->widget(DatePicker::classname(), [
            'options' => ['placeholder' => 'Input Date ...'],
            'pluginOptions' => [
                'autoclose'=>true,
                'format' => 'yyyy-mm-dd'
            ]
        ]);
    ?>

    <?= $form->field($model, 'image_id')->widget(FileInput::classname(), [
              'options' => ['accept' => 'image/*'],
               'pluginOptions'=>['allowedFileExtensions'=>['jpg','gif','png'],'showUpload' => false,],
          ]);   ?>


   <!--  

   

    <?= $form->field($model, 'key_code')->textInput(['maxlength' => true]) ?>

    

    <?= $form->field($model, 'due_date')->textInput() ?>

    <?= $form->field($model, 'billing_date')->textInput() ?>

    <?= $form->field($model, 'api_url')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'billing_type')->textInput() ?>

    

    <?= $form->field($model, 'bill_status')->textInput() ?>

    <?= $form->field($model, 'comp_email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'active')->checkbox() ?>

    <?= $form->field($model, 'image_id')->textInput(['maxlength' => true]) ?> -->

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
