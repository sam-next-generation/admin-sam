<?php

namespace frontend\controllers;

use common\models\City;
use Yii;
use common\models\Branch;
use common\models\search\BranchSearch;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BranchController implements the CRUD actions for Branch model.
 */
class BranchController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Branch models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BranchSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Branch model.
     * @param string $branch_code
     * @param string $comp_id
     * @return mixed
     */
    public function actionView($branch_code, $comp_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($branch_code, $comp_id),
        ]);
    }

    /**
     * Creates a new Branch model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Branch();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Branch model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $branch_code
     * @param string $comp_id
     * @return mixed
     */
    public function actionUpdate($branch_code, $comp_id)
    {
        $model = $this->findModel($branch_code, $comp_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Branch model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $branch_code
     * @param string $comp_id
     * @return mixed
     */
    public function actionDelete($branch_code, $comp_id)
    {
        $this->findModel($branch_code, $comp_id)->delete();

        return $this->redirect(['index']);
    }
    /**
     * Finds the Branch model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $branch_code
     * @param string $comp_id
     * @return Branch the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($branch_code, $comp_id)
    {
        if (($model = Branch::findOne(['branch_code' => $branch_code, 'comp_id' => $comp_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionCity(){
        $out = [];

        if(isset($_POST['depdrop_parents'])){
            $parents = $_POST['depdrop_parents'];
            if($parents != null){
                $prov_id = $parents[0];
                $out = City::find()->select(['city_code id','city_name name'])->where(['province_code' => $prov_id])->asArray()->all();
                echo Json::encode(['output' => $out, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output'=>'', 'selected'=>'']);
    }
}
