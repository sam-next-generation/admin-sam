<?php

namespace frontend\controllers;

class ParentController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

}
