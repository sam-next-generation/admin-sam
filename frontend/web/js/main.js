var dataTable;
var base_url = '//'+window.location.host + '/admin-sam';
toastr.options = {
  "closeButton": false,
  "debug": false,
  "newestOnTop": false,
  "progressBar": false,
  "positionClass": "toast-top-right",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
};
$(function(){
  $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
  MINOVATE.extra.init();
  $(document).ready(function(){
     MINOVATE.documentOnReady.init();
  });
  $(window).on( 'resize', MINOVATE.documentOnResize.init );
  window.onpopstate = function(event) {
     location.reload();
  };
})